using ITUtil.Common.Command;
using ITUtil.Common.MicroserviceBootstrapper;
using ITUtil.Common.RabbitMQ.DTO;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Unittests
{
    [TestFixture]
    public class ServiceMessageHandlersTest
    {
        [Test]
        public void TEST()
        {
            ServiceMessageHandlers handler = new ServiceMessageHandlers("test", new TestNamespace());
            var ts = handler.ScarfoldTS(new string[] { "Test", "scarfoldTS" }, "", new TokenInfo(), DateTime.Now);

            Assert.IsNotNull(ts, "ts should not be null");
        }
    }


    public class TestNamespace : INamespace
    {
        public string Name { get { return "TEST"; } } //ER DENNE NěDVENDIG!!!???

        public List<ICommandBase> Commands { get; set; }

        public ProgramVersion ProgramVersion { get { return new ProgramVersion("1.0.0.0"); }  }

        public TestNamespace()
        {
            this.Commands = new List<ICommandBase>();
            new TestCommand(this);
            new Test2Command(this);
        }
    }
    public class TestCommand : GetCommand<Test.DTO.v1.DTOERNE.inputDTO, Test.DTO.v1.DTOERNE.outputDTO>
    {
        public TestCommand(INamespace inamespace) : base()
        {
            this.Claims = new List<Claim>() { };
            this.Description = "Set the settings for this micro service, " + inamespace.Name + ".";
        }

        public override string Description { get; }

        public override List<Claim> Claims { get; }

        public override Test.DTO.v1.DTOERNE.outputDTO Execute(Test.DTO.v1.DTOERNE.inputDTO input)
        {
            return null;
        }
    }
    public class Test2Command : GetCommand<Test.DTO.v1.DTOERNE.input2DTO, Test.DTO.v1.DTOERNE.outputDTO>
    {
        public Test2Command(INamespace inamespace) : base()
        {
            this.Claims = new List<Claim>() { };
            this.Description = "Set the settings for this micro service, " + inamespace.Name + ".";
        }

        public override string Description { get; }

        public override List<Claim> Claims { get; }

        public override Test.DTO.v1.DTOERNE.outputDTO Execute(Test.DTO.v1.DTOERNE.input2DTO input)
        {
            return null;
        }
    }


}
namespace Test.DTO.v1.DTOERNE
{
    public class inputDTO
    {
        public string input { get; set; }
    }
    public class input2DTO
    {
        public inputDTO input { get; set; }
        public string name { get; set; }
        public int id { get; set; }
    }
    public class outputDTO
    {
        public string output { get; set; }
        public inputDTO inputdto { get; set; }
    }
}