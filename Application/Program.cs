﻿
namespace ITUtil.Common.MicroserviceBootstrapper
{
    using ITUtil.Common.Command;
    using ITUtil.Common.RabbitMQ;
    using ITUtil.Common.RabbitMQ.Common.Event;
    using ITUtil.Common.RabbitMQ.Event;
    using ITUtil.Common.RabbitMQ.Message;
    using ITUtil.Common.RabbitMQ.Request;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    internal class Program
    {
        public static ProgramVersion programVersion { get { return new ProgramVersion("1.0.0.1"); } }
        private static string AddInFolder = "";
        private static string assemblyFile = "";
        public static string machineId = "";

        private static void Main(string[] args)
        {
            AddInFolder = args[0];
            assemblyFile = args[1];
            if (args.Length > 2)
            {
                machineId = args[2];
            }

            var dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); //System.IO.Directory.GetCurrentDirectory()
            System.IO.Directory.SetCurrentDirectory(dir + @"/" + AddInFolder);
            Console.Title = args[1];

            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            Assembly thisAssem = Assembly.LoadFrom(assemblyFile);
            AssemblyName thisAssemName = thisAssem.GetName();
            Type[] types = thisAssem.GetTypes();

            List<INamespace> INamespaceInstances = new List<INamespace>();
            var INamespaceImplementation = thisAssem.GetTypes()
              .Where(x => typeof(INamespace).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
              .Select(x => x)
              .ToList();
            foreach (var impl in INamespaceImplementation)
            {
                INamespace instance = (INamespace)Activator.CreateInstance(impl);
                INamespaceInstances.Add(instance);
            }

            // start the request server (for handling incomming requests)
            List<MessageQueueConfiguration> configurations = new List<MessageQueueConfiguration>();

            var EventListenersImplementation = thisAssem.GetTypes()
                            .Where(x => typeof(IEventListener).IsAssignableFrom(x) && !x.IsInterface && !x.IsAbstract)
                            .Select(x => x)
                            .ToList(); //could be a list of them, but then we would not know which to call?
            var EventListeners = new List<IEventListener>();
            foreach (var impl in EventListenersImplementation)
            {
                var instance = Activator.CreateInstance(impl) as IEventListener;
                EventListeners.Add(instance);
            }

            foreach (var Instance in EventListeners)
            {
                configurations.Add(new EventConfiguration(
                Instance.ServiceName.ToLower(),
                new List<string>() { Instance.ServiceName.ToLower() + ".#" },
                new Constants.EventHandler(Instance.Handler)
                ));
            }


            foreach (var Namespace in INamespaceInstances)
            {
                var servicename = Namespace.Name.ToLower();// Namespace.GetType().Name.ToLower();

                var serviceMessageHandler = new ServiceMessageHandlers(servicename, Namespace);
                configurations.Add(new RequestConfiguration(//for handling routes that are cross-version (such as help and service discovery)
                    servicename + "_*",
                    new List<string>() { servicename + ".help", servicename + ".scarfoldTS" },
                    serviceMessageHandler.HandleMessage,
                    true)
                    );

                configurations.Add(new RequestConfiguration(//for handling routes that are cross-version (such as help and service discovery)
                    servicename + "_servicediscovery_*",
                    new List<string>() { "servicediscovery.#" },
                    serviceMessageHandler.HandleMessage,
                    true)
                    );

                configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                servicename + $".v{Namespace.ProgramVersion.major}",
                new List<string>() { servicename + $".v{Namespace.ProgramVersion.major}" + ".claims" },
                new MessageHandlers(Namespace).HandleMessage)
                );

                configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                servicename + $".v{Namespace.ProgramVersion.major}",
                new List<string>() { servicename + $".v{Namespace.ProgramVersion.major}" + ".eventCallback" },
                new MessageHandlers(Namespace).HandleMessage)
                );

                configurations.Add(new RequestConfiguration(//for handling routes that are noncross-version (such as the service itself)
                servicename + $".v{Namespace.ProgramVersion.major}",
                Namespace.Commands.Select(p=> servicename + $".v{Namespace.ProgramVersion.major}"+"." + p.GetType().Name).ToList(),
                new MessageHandlers(Namespace).HandleMessage)
                );

                var helpdto = new ServiceMessageHandlers(servicename, Namespace).GetHelpDTO();
                EventClient.Instance.RaiseEvent("servicediscovery", new ReturnMessage(true, helpdto));
            }


            using (var server = new RequestServer(configurations))
            {
                Console.ReadLine();
            }
        }

        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {//ISSUE: Publish mappen indeholder ikke dll'er som common afhænger af... = crash hvis man ikke manuelt kopier dem ind
         //ISSUE: publish mappen indeholder ikke configurations filen hvis man ikke manuelt kopiere den ind
            Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
            if (assembly != null)
                return assembly;
            string filename = args.Name.Split(',')[0] + ".dll".ToLower();

            return LoadAssembly("."/*AddInFolder*/, filename);
        }

        private static Assembly LoadAssembly(string folder, string filename)
        {
            string asmFile = System.IO.Path.Combine(folder, filename);
            if (System.IO.File.Exists(asmFile))
            {
                try
                {
                    return System.Reflection.Assembly.LoadFrom(asmFile);
                }
                catch
                {
                    //hide exception, as we will try to load another version if available...
                }
            } 
            foreach (var dir in System.IO.Directory.GetDirectories(folder))
            {
                var res = LoadAssembly(dir, filename);
                if (res != null)
                {
                    return res;
                }
            }
            return null;
        }
    }
}
