## Introduction

Servicewrapper for IT-Util is a console execution wrapper around IMicroservice implementations. 
The wrapper initializes the interface and handles communication with other services
and the gateway.

# Overview

IT-Util provides the core requirements for distributed systems development including RPC and Event driven communication. 
The micro philosophy is sane defaults with a pluggable architecture. 
We provide defaults to get you started quickly but everything can be easily swapped out.
